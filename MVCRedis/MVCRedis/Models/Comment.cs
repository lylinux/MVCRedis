﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCRedis.Models
{
    public class Comment
    {
        public long Id { get; set; }
        public string title { get; set; }
        public string Content { get; set; }
        public long UserID { get; set; }
    }
}