﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCRedis.Models
{
    public class Post
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public List<long> CommentId { get; set; }
        public long UserId { get; set; }
    }
}