﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCRedis.Models;
using CommonHelper.Common;
using CommonHelper;
using RedisHelper.Config;
namespace MVCRedis.BLL
{
    public class UserDB
    {
        private string key = RedisConfig.Key;
        public bool RegisterUser(User u)
        {

            try
            {
                u.Id = RedisHelperMethods.GetNextSequence<User>();
                RedisHelperMethods.SetEntity<User>(key, u);
                return true;
            }
            catch (System.Exception)
            {
                throw;
            }


        }

        public bool CheckEmail(string email)
        {
            var res = RedisHelperMethods.SearchEntitys<User>(u => u.Email == email);
            return res.Count() == 0;
        }
    }
}
