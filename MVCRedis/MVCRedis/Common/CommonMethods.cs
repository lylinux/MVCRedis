﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCRedis.Models;
using CommonHelper;

namespace MVCRedis.Common
{
    public class CommonMethods
    {
        private static string key = "MVCRedis";
        public static void InitModels()
        {
            var u = new User() { Id = 1, Name = "zhangsan", Email = "liang@liang.com", Password = "123" };
            var c = new Comment() { Id = 1, title = "comment", Content = "content", UserID = u.Id };
            var p = new Post() { Id = 1, Name = "test", Content = "testcontent", UserId = u.Id,CommentId=new List<long>() };
            p.CommentId.Add(c.Id);
            CommonHelper.RedisHelperMethods.StroteEntity<User>(u);
            CommonHelper.RedisHelperMethods.StroteEntity<Comment>(c);
            CommonHelper.RedisHelperMethods.StroteEntity<Post>(p);
        }

        public static void TestSet()
        {
            var u = new User() { Id = RedisHelperMethods.GetNextSequence<User>(), Name = "zhangsan1", Email = "liang@liang.com1", Password = "123" };
            var c = new Comment() { Id = RedisHelperMethods.GetNextSequence<Comment>(), title = "comment", Content = "content", UserID = u.Id };
            var p = new Post() { Id = RedisHelperMethods.GetNextSequence<Post>(), Name = "test", Content = "testcontent", UserId = u.Id, CommentId = new List<long>() };
            p.CommentId.Add(c.Id);
            RedisHelperMethods.SetEntity<User>(key, u);
            RedisHelperMethods.SetEntity<Comment>(key, c);
            RedisHelperMethods.SetEntity<Post>(key, p);
            var u1 = RedisHelperMethods.GetEntityById<User>(key, u.Id);
            var c1 = RedisHelperMethods.GetEntityById<Comment>(key, c.Id);
            var p1 = RedisHelperMethods.GetEntityById<Post>(key, p.Id);
            u1.Email = "FFF@ff.com";
            RedisHelperMethods.UpdateEntity<User>(key, u1);

            var us = RedisHelperMethods.GetEntitys<User>(key);
            var ps = RedisHelperMethods.GetEntitys<Post>(key);
            var cs = RedisHelperMethods.GetEntitys<Comment>(key);
            var ff = RedisHelperMethods.GetPagedList<User>(key, -1, 3);
        }



        public static List<T> GetEntitys<T>()
        {
            var result = CommonHelper.RedisHelperMethods.GetEntitys<T>();
            return result;
        }

       
    }
}