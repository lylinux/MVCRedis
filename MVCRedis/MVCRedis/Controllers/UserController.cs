﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCRedis.Models;
using MVCRedis.BLL;
namespace MVCRedis.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        private UserDB db = new UserDB();
        [HttpPost]
        public ActionResult Register(string name, string email, string password, string confirmPassword)
        {
            JsonResult jr = new JsonResult();
            if (password != confirmPassword)
            {
                jr.Data = new { res = false, msg = "两次输入密码不一致" };
                ViewBag.json = jr;
                return View();
            }
            try
            {
                db.RegisterUser(new Models.User { Name = name, Email = email, Password = password });
            }
            catch (System.Exception ex)
            {
                jr.Data = new { res = false, msg = "存取发生异常!" };
                ViewBag.json = jr;
                return View();
            }
            jr.Data = new { res = true, msg = string.Empty };
            ViewBag.json = jr;
            return View();
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult CheckEmail()
        {
            string email = Request.Form["email"];
            JsonResult jr = new JsonResult();
            if (db.CheckEmail(email))
            {
                jr.Data = new { valid = true, message = "该电子邮件地址可用" };
            }
            else
            {
                jr.Data = new { valid = false, message = "该电子邮件地址不可用" };
            }
            return jr;
        }
    }
}
